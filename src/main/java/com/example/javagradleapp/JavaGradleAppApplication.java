package com.example.javagradleapp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class JavaGradleAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(JavaGradleAppApplication.class, args);
    }

    @PostConstruct
    public void init()
    {
        Logger log = LoggerFactory.getLogger(JavaGradleAppApplication.class);

        try {
            log.info("I am a Java app");
            log.info("Just logging stuff");

            log.info("made some changes");

            throw new NullPointerException("Ooh noes! Something bad happened");
        }
        catch (Exception e) {
            log.error("Error occurred!", e);
        }
    }

}
