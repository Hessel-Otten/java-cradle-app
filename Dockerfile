FROM openjdk:8-jre-alpine

WORKDIR /usr/app

COPY ./build/libs/java-app-0.0.1-SNAPSHOT.jar .

ENTRYPOINT ["java", "-jar", "java-app-0.0.1-SNAPSHOT.jar"]